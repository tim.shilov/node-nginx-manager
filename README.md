# node-nginx-man

Nginx-man is a simple NGINX configuration manager for Node.js apps.

## Installation

nginx-man requires [Node.js](https://nodejs.org/) v8+ to run.

Install dependency.

```sh
npm install nginx-man
```

Usage example

```javascript
const nginxMan = require('nginx-man');

nginxMan.updateConfig({
    proxy_servers: [
        { domain: 'example.domain.com', port: 8080 },
    ],
});
```

## Development

Want to contribute? Great!
Feel free to submit issues or pull requests.
