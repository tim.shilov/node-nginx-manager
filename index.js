const { NGINX_MAN } = require('./lib/nginx');

const { updateConfig } = NGINX_MAN;

// Public API
module.exports = {
    updateConfig,
};
