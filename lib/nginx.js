const fs = require('fs-extra');
const path = require('path');
const os = require('os');
const { execSync } = require('child_process');

const PROJECT_ROOT = path.resolve(__filename, '../../');

const NGINX_ROOT = '/etc/nginx';
const LOCK_FILENAME = '/tmp/node-nginx-man.lock';

const PATHS = {
	SITES_AVAILABLE: path.resolve(NGINX_ROOT, 'sites-available'),
	SITES_ENABLED: path.resolve(NGINX_ROOT, 'sites-enabled'),
	CONF_D: path.resolve(NGINX_ROOT, 'conf.d'),
};

function checkFolderStructure() {
	const paths = [
		NGINX_ROOT,
		PATHS.CONF_D,
		PATHS.SITES_AVAILABLE,
		PATHS.SITES_ENABLED
	];

	paths.forEach(path => {
		if(!fs.existsSync(path)) {
			fs.mkdirSync(path);
		}
	});
}

async function createServerBlock(domain = 'example.domain.com', port = 8080, errorPages) {
	const errorsPlaceholder = '# ERRORS BELOW';
	const fillDomain = (str) => str.replace(new RegExp('%DOMAIN%', 'g'), domain);
	const sslPath = fillDomain('/etc/letsencrypt/live/%DOMAIN%/privkey.pem');
	const sslKeyExists = fs.existsSync(sslPath);

	let serverBlock = await fs.readFile(
		path.resolve(
			PROJECT_ROOT,
			'templates',
			`server_proxy_block_${sslKeyExists ? 'https' : 'http'}.conf`
		),
		'utf8'
	);

	serverBlock = fillDomain(serverBlock).replace(new RegExp('%PORT%', 'g'), port);

	if(errorPages) {
		errorPages.forEach((errorPage) => {
			const {code, pageFilename, rootPath} = errorPage;
			const errorPageBlock = createErrorPageBlock(code, pageFilename, rootPath);
			serverBlock = serverBlock.replace(errorsPlaceholder, [errorsPlaceholder, errorPageBlock, ''].join('\n'));
		});
	}

	return serverBlock;
}

function createErrorPageBlock(code, pageFilename, rootPath) {
	return `error_page ${code} /${pageFilename};

    location /${pageFilename} {
        root ${rootPath};
    }`;
}

function createLock() {
	if(fs.existsSync(LOCK_FILENAME)) {
		return false;
	}

	fs.writeFileSync(LOCK_FILENAME, '');
	return true;
}

const NGINX_MAN = {
	async updateConfig(options = { proxy_servers: [] }) {
		if(os.platform() !== 'linux' && !options.ignoreOSCheck) {
			console.log('Your system is not supported. `nginx-man` currently only works on Linux');
			return;
		}

		if(!fs.existsSync(NGINX_ROOT)) {
			console.log('NGINX folder not found. Ignoring...');
			return;
		}

		try {
			const lockCreated = createLock();
			if(!lockCreated) {
				console.log(`Lock file exists: ${LOCK_FILENAME}.\nIf you sure no other nginx-man instances are running then remove this file and try again.`);
				return;
			}

			// check that all folders exist
			checkFolderStructure();

			// copy "main configs"
			fs.copySync(path.resolve(PROJECT_ROOT, 'configs'), NGINX_ROOT, { overwrite: true });

			// create server blocks
			for(let server of options.proxy_servers) {
				const { domain, port, errorPages } = server;
				const serverBlockFilename = path.resolve(
					PATHS.SITES_AVAILABLE,
					domain
				);
				const serverBlock = await createServerBlock(domain, port, errorPages);

				fs.writeFileSync(serverBlockFilename, serverBlock);
				fs.ensureSymlinkSync(
					serverBlockFilename,
					path.resolve(PATHS.SITES_ENABLED, domain)
				);
			}

			// testing new configs (NOTE: this will throw an error if config test fails)
			execSync('nginx -t');
			// reloading NGINX for new configs to take effect
			execSync('systemctl reload nginx');
		} catch(error) {
			console.error(error);
		} finally {
			fs.removeSync(LOCK_FILENAME);
		}
	}
};

module.exports = {
	NGINX_MAN
};
